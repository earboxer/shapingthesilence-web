<?php
/**
 * @brief Creates a widget showing if a twitch user is currently live
 * Copyright (C) 2017-2018 Zach DeCook
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
require_once('vendor/autoload.php');
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

/**
 * @brief return status, or NULL if offline.
 */
function sts_status(&$content)
{
	//mx4bzuy130npdgsjeu1qy12lhkonkm
	// Client ID for Shaping The Silence widget
	// V3 api will be deprecated 12/31/2018
	// Using for now, since V5 api has horrible documentation.
	//https://api.twitch.tv/kraken/channels/shapingthesilence?client_id=mx4bzuy130npdgsjeu1qy12lhkonkm
	//73518616
	//<script src="https://api.twitch.tv/kraken/streams/shapingthesilence?client_id=...&callback=badge.drawStream" type="text/javascript"></script>
	$clientID = $_ENV['TWITCH_V3_API_CLIENT_ID'];
	$url = "https://api.twitch.tv/kraken/channels/shapingthesilence?client_id=$clientID";
	// Keep this data. url, profile_banner, logo, game, display_name
	$content = json_decode( file_get_contents( $url ) );
	$url2 = "https://api.twitch.tv/kraken/streams/shapingthesilence?client_id=$clientID";
	$stream = json_decode( file_get_contents( $url2 ) );
	return isset( $stream->stream ) ? $content->status : NULL;
}

function sts_status_bar()
{
	$content = NULL;
	if ( sts_status( $content ) )
	{
		$status = htmlentities( $content->status );
		// URL should never contain a single tick.
		$url = str_replace( "'", '', $content->url );
		$display_name = htmlentities( $content->display_name );
		$css = "<link rel='stylesheet type='text/css' href='/res/widget.css'>";
		return "$css<a href='$url' id='twitch-widget'>🔴 $display_name Live Now - $status</a>";
	}
	return '';
}

echo sts_status_bar();
