
<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw sts-loading" style='display:none;color: rgba(255,255,255,0.75);'>
	<span class="sr-only">Loading...</span>
</i>

<div id='contentbox'>

<div class="content content-left w3-half w3-row-padding col-lg-4	col-md-5 col-sm-10	col-xs-12 col-lg-offset-1	col-md-offset-1 col-sm-offset-1 ">

##  Featured Songs

<iframe title="Bandcamp Widget -- Listen to Aesthetic Appeal" style="border: 0; width: 100%; max-width: 450px; height: 700px;" src="https://bandcamp.com/EmbeddedPlayer/album=2077129398/size=large/bgcol=333333/linkcol=40669b/transparent=true/" seamless><a href="http://music.shapingthesilence.com/album/aesthetic-appeal">Aesthetic Appeal by Shaping The Silence</a></iframe>

</div>
<div class='content content-right w3-half w3-row-padding col-lg-4	col-md-5 col-sm-10	col-xs-12 col-lg-offset-2	col-md-offset-0 col-sm-offset-1'>
##  About Shaping The Silence{#about}

Shaping The Silence is a project that blurs the lines of electronic music
genres while simultaneously fusing it with orchestral instrumentation for a
cinematic experience that takes the listener along for a dynamic and emotional
journey. This theme has been growing and evolving since its creator, Jared
DeCook, began releasing tracks under this title in 2012.

[Links](links){.keepplaying}

</div>

</div>

<div id='loadzone' class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
<script src="/res/keepplaying.js"></script>
<script src="https://zachdecook.com/scripts/modernizr.js"></script>
