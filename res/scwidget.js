var scw;
var iframeElement;
jQuery(function(){
	iframeElement = document.querySelector('iframe.sc');
	console.log(iframeElement);
	scw = SC.Widget(iframeElement);

	scw.bind(SC.Widget.Events.PLAY, scplaycallback)
	scw.bind(SC.Widget.Events.PAUSE, scpausecallback)
});

function scplaycallback()
{
	jQuery("#scplaybutton").remove();
	jQuery('<a/>', {
		id: 'scplaybutton',
		class: 'fa fa-play-circle-o fa-spin fa-3x fa-fw',
		style: 'color:rgba(255,255,255,0.75);left:2%;top:2%;position:fixed;',
		href: '#',
	}).appendTo('body');
	jQuery("#scplaybutton").click(function(event){event.preventDefault();scw.pause();});
}

function scpausecallback()
{
	jQuery("#scplaybutton").removeClass('fa-spin');
	jQuery("#scplaybutton").click(function(event){event.preventDefault();scw.play();});
}
