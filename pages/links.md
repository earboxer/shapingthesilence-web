<div class='content links' id='loadzone'>

## Newsletter Signup

<!-- Begin Mailchimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.cssx" rel="stylesheet" type="text/css">
<div id="mc_embed_signup">
<form action="https://shapingthesilence.us2.list-manage.com/subscribe/post?u=5cb9f79c5dd60db8f35998fbf&amp;id=97e89f1516" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
<div id="mc_embed_signup_scroll">
<input type="email" value="" name="EMAIL" class="email w3-threequarter" id="mce-EMAIL" placeholder="email address" required><!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups--><div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_5cb9f79c5dd60db8f35998fbf_97e89f1516" tabindex="-1" value=""></div><div class="clear w3-quarter"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
</div>
</form>
</div>
<!--End mc_embed_signup-->

<br/>

## Listen

<!--[<i class="fa fa-soundcloud"></i>Soundcloud](https://soundcloud.com/shapingthesilence)-->
[<i class="fa fa-spotify"></i>Spotify](https://open.spotify.com/artist/3pNj3dPmzhsxLhhOmXejQ8)
[<i class="fa fa-apple"></i>Apple Music](https://itun.es/us/NUXjT)
[<i class="fa fa-youtube-play"></i>YouTube](https://www.youtube.com/user/ShapingTheSilence)

## Download

[<i class="fa fa-bandcamp"></i>Bandcamp](http://music.shapingthesilence.com)
[<i class="fa fa-play"></i>Google Play](https://play.google.com/store/music/artist/Shaping_The_Silence?id=A2fvbtfwpuppajsbv5tjabxsetu)
[<i class="fa fa-apple"></i>iTunes](https://itunes.apple.com/us/artist/shaping-the-silence/id757537703)

## Connect

[<i class="fa fa-twitch"></i>Twitch](https://www.twitch.tv/shapingthesilence)
[<i class="fa fa-twitter"></i>Twitter](https://twitter.com/shapingsilence)
[<i class="fa fa-facebook"></i>Facebook](https://www.facebook.com/ShapingTheSilence/)

</div>
