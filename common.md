---
title: 'Shaping The Silence'
lang: en-US
header-includes:
    <meta name="theme-color" content="#40669b">
    <meta name="description" content="Shaping The Silence blurs the lines of electronic music genres fusied with orchestral instrumentation for a cinematic experience, taking the listener along for a dynamic and emotional journey.">
    <link rel="stylesheet" type="text/css" href="https://zachdecook.com/shared/w3css/4/w3.css"/>
    <link rel="stylesheet" type="text/css" href="/res/sts.css?4"/>
	<link rel="stylesheet" type="text/css" href="/res/scwidget.css"/>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.png" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
include-after:
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script type="text/javascript">
    function checklive(){
    $($( "#livenow" ).load( "/livenow.php" ));
    }
    checklive();
    </script>
---

<div id='livenow'></div>

[ ![Shaping The Silence](/res/sts.svg){.logo}
](/)

[<i title="Twitter Profile" class="fa fa-twitter social"></i>](https://twitter.com/shapingsilence)
[<i title="Facebook Page" class="fa fa-facebook social"></i>](https://www.facebook.com/ShapingTheSilence/)
[<i title="Twitch Livestreams" class="fa fa-twitch social"></i>](https://www.twitch.tv/shapingthesilence)
[<i title="Bandcamp Store" class="fa fa-bandcamp social"></i>](http://music.shapingthesilence.com)
