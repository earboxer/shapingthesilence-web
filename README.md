# Shaping The Silence Webpage

## Setup

Clone this repository using `git clone https://gitlab.com/earboxer/shapingthesilence-web.git`

### Static File Serving

Run `git-hooks/post-merge` to create the static html files.
This renders the files from pages/*.md into html.

This can be installed as a git-hook with `ln -s git-hooks/post-merge .git/hooks/`,
so that the site will update whenever you `git pull`.

### Twitch "Live Now" Popup

#### PHP

Since the file is php, you'll need to configure apache to load the php module,
or nginx to run php-fpm.

#### Composer

You need to run `composer install` to get the dependecy to read the .env file.

[more about installing composer...](https://getcomposer.org/doc/00-intro.md)

#### Twitch Developer Application Client ID

You need a Twitch Developer Application client ID.

To get one, you'll have to register as a developer,
register an 'application', and copy the client id from there.

https://glass.twitch.tv/console/apps

Put the client id in `.env`, following the example of `.env.example`

Then, /livenow.php should show nothing if he's not live, and show something if he is live.
