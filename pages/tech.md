---
title: Shaping The Silence Hardware Projects
date: 2015-11-01
publishedat: <http://shapingthesilence.com/tech>
#modified: 2017-09-02
author: Zach DeCook
---

<div class="contentbox">

<div
class="content techpage col-lg-8 col-md-10 col-sm-10 col-xs-12 col-lg-offset-2 col-md-offset-1 col-sm-offset-1">

### Indistinguishable from Magic

Hardware problem, or software problem? In our experience building custom
gear, we’ve learned a thing or two from the process.

1.  Everything you need to know to solve your problems can usually be
    found in a large PDF file readily available on the internet.
2.  No really, read the datasheet.

I could fill this intro by talking about my taste in capacitors, or a
shocking story about disposable cameras, but that mightn’t be very
professional. Anyway, here are some of our projects.

### Life Box

Features wireless MIDI sync and LED control, motion controlled strobe
light, and a randomize feature that will keep the audience jumping.

![](https://68.media.tumblr.com/9c7aacfc491e157a1730c0251ce9903c/tumblr_inline_nwyvthB28v1s1int4_500.jpg)

Transform DJ’s biggest concern was how to get this through airport
security.

![](https://68.media.tumblr.com/428f51a22c6ab9cea63e3e63163f061c/tumblr_inline_nwyuyjJAW71s1int4_500.jpg)

The wireless receiver. (Some day I hope to get a second extruder and
conductive filament, so I can 3d print circuitboards into their cases.)

Can be seen at the 44 second mark, and at various points throughout.

### Combo Breaker

The magic of multiplexing. What do you do when you’re out of pins on
your microcontroller? Multiplex! When great power met irresponsibility
(and a really good sale on the highest quality arcade buttons), this
project was born. The layout is based on the [harmonic
table,](https://en.wikipedia.org/wiki/Harmonic_table_note_layout) and
the lights help you navigate it by indicating an octave above or
below.[![](https://68.media.tumblr.com/9da8cb97d8822f2e927e4f721f048870/tumblr_inline_nwyqrvNZEw1s1int4_500.jpg)](https://en.wikipedia.org/wiki/Harmonic_table_note_layout)

![](https://68.media.tumblr.com/d45b99517730d403249e6c5c202b49fc/tumblr_inline_nwyqswWrKZ1s1int4_500.jpg)

(Jared’s really proud of the inside)

Here we are recording with it.

### guitar\_2 aka project naira

Don’t ever let anyone tell you EL wire isn’t cool.

![](https://68.media.tumblr.com/8a2a622da757fcace19682bccfd3523a/tumblr_inline_nwytppk0TA1s1int4_500.jpg)

This was a lot of fun to program. Along with including 30 different
musical scales, I enjoyed making an algorithm to generate chords from
each scale. Along with chord mode, there’s a note mode, where you can
use a binary pattern to play different notes. This thing is proof that
guitar hero controllers can have a decent life after retirement.

![](https://68.media.tumblr.com/c77275f697de05c709c156c596728d8b/tumblr_inline_nwyrukJEQy1s1int4_500.jpg)

Jared surviving his first show (before the guitar was wireless).

“Wireless is so much better because you don’t have random USB
disconnection issues.” - Jared

“The future of technology … is wireless. You know, things like walkmans,
flashlights, and solar calculators.” - Strong Bad

For inquiries, email shapingthesilence@gmail.com

</div>

</div>
