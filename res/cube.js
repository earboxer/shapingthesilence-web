
function bindLinks(){
	$('a.keepplaying').bind( "click", function(e){
		e.preventDefault();
		_link = $(this).closest("[href]").attr("href");
		if(Modernizr.history){
			history.pushState(null,null,_link);
		}
		loadContent(_link);
	});
}

function bindCube(){
	$('#cube1').bind( "click", function(e){

		//if( $("#cube1").attr('src') != "res/cube24.png")
			e.preventDefault();

		if( $("#cube1").attr('src') == "res/cube2.png")
			$("#cube1").attr('src', "res/cube22.png");
		else if( $("#cube1").attr('src') == "res/cube22.png")
			$("#cube1").attr('src', "res/cube23.png");
		else if( $("#cube1").attr('src') == "res/cube23.png"){
			$("#cube1").attr('src', "res/cube24.png");
		}
		else if( $("#cube1").attr('src') == "res/cube24.png"){
			_link = $("#cube1").closest("[href]").attr("href");
			if(Modernizr.history){
				history.pushState(null,null,_link);
			}
			loadContent(_link);
		}
	});
}

$( document ).ready( bindCube );
$(document).ready( bindLinks);
$(function()
{
	if(Modernizr.history){
			$(window).bind('popstate', function(){
			_link = location.pathname.replace(/^.*[\\\/]/, ''); //get filename only
			loadContent(_link);
		});
	}
	$(".logo").bind( "click", function(e){
		e.preventDefault();
		_link = $(".logo").closest("[href]").attr('href');
		if(Modernizr.history){
			//TODO: Don't add to the history stack if we're already on the page.
			history.pushState(null,null,_link);
		}
		loadContent(_link);
	});
});

function loadContent( href ){
	$('.sts-loading').show();
	$("#loadzone").load(href + " #loadzone", null, function(){
		bindCube();
		$('.sts-loading').hide();
	});
	// If they want to go back to the homepage, just show what was previously hidden.
	if( href == '/' || href == '' ){
		$("#contentbox").show();
	}
	else
		$("#contentbox").hide();

}
